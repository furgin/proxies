package au.com.angry.proxy.cli;

import au.com.angry.proxy.Cache;
import au.com.angry.proxy.Deck;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.net.MalformedURLException;
import java.net.URL;

public class DeckProxy {
    public static void main(String[] args) {

        Option outfile = Option.builder("o")
                .argName("outfile")
                .hasArg()
                .desc("name of the proxy output file")
                .longOpt("outfile")
                .build();

        Option cachedir = Option.builder()
                .argName("cache")
                .hasArg()
                .desc("cache directory")
                .longOpt("cache")
                .build();

        Option help = new Option("h", "help", false, "print this message");
        Option report = new Option("r", "report", false, "include report");

        Options options = new Options();
        options.addOption(outfile);
        options.addOption(help);
        options.addOption(report);
        options.addOption(cachedir);

        CommandLineParser parser = new DefaultParser();
        try {
            // parse the command line arguments
            CommandLine line = parser.parse(options, args);
            boolean invalid = false;

            if (line.getArgList().size() != 1) {
                invalid = true;
            } else {
                String url = line.getArgList().get(0);
                try {
                    new URL(url);
                } catch (MalformedURLException e) {
                    System.out.println(String.format("invalid url: %s", url));
                    System.out.println();
                    invalid = true;
                }
            }

            if (invalid || line.hasOption("help")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("proxies [options] url", options);

            } else {

                if(line.hasOption("cache"))
                    Cache.setDirectory(line.getOptionValue("cache"));

                String url = line.getArgList().get(0);
                Deck deck = new Deck.Builder()
                        .load(url).build();

                if (line.hasOption("report"))
                    deck.generateReport();

                if (line.hasOption("outfile"))
                    deck.generatePDF(line.getOptionValue("outfile"));
            }

        } catch (ParseException exp) {
            // oops, something went wrong
            System.err.println("Parsing failed.  Reason: " + exp.getMessage());
        }


    }
}
