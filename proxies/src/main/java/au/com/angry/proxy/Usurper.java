package au.com.angry.proxy;

import au.com.angry.proxy.elements.TextElement;
import au.com.angry.proxy.frames.SimpleProxyFrame;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.stream.Stream;

public class Usurper {

    private static Proxy.Builder king() {
        return ruler()
                .withName("King")
                .withType("Role - King");
    }

    private static Proxy.Builder queen() {
        return ruler()
                .withName("Queen")
                .withType("Role - Queen");
    }

    private static Proxy.Builder ruler() {
        return new Proxy.Builder()
                .withRules("Keep this role face up at all times.\n" +
                        "You start with 60 life.\n" +
                        "You are the first player.\n" +
                        "You win the game if you have no opponents besides Guards.")
                .withFlavor("In the End, we will remember not the words of our enemies, but the silence of our friends.");
    }

    private static Proxy.Builder usurper() {
        return new Proxy.Builder()
                .withType("Role - Usurper")
                .withName("Usurper")
                .withRules("You can reveal this role at any time.\n" +
                        "If you control the effect that caused the King or Queen to die, instead of dying " +
                        "the King or Queen exchanges roles with you.  Their life total becomes 1, and your " +
                        "life total becomes 60.  You both lose all commander damage and poison counters.")
                .withFlavor("When you play the game of thrones, you win or you die.");
    }

    private static Proxy.Builder assassin() {
        return new Proxy.Builder()
                .withType("Role - Assassin")
                .withName("Assassin")
                .withRules("Keep this role face down at all times.\n" +
                        "When the King or Queen dies, if at least one Assassin is still playing you win the game.\n" +
                        "You do not lose the game by being eliminated.")
                .withFlavor("Kill a man, and you are an assassin.  Kill millions of men, and you are a conqueror.  Kill everyone, and you are a god.");
    }

    private static Proxy.Builder traitor() {
        return new Proxy.Builder()
                .withType("Role - Traitor")
                .withName("Traitor")
                .withRules("Keep this role face down at all times.\n" +
                        "When the King or Queen dies, if you have no other opponents, you win the game.")
                .withFlavor("If opportunity doesn't knock, build a door.");
    }

    private static Proxy.Builder guard() {
        return new Proxy.Builder()
                .withType("Role - Guard")
                .withName("Guard")
                .withRules("Keep this role face down at all times.\n" +
                        "You do not lose the game by being eliminated.\n" +
                        "You win the game when the King or Queen wins the game.")
                .withFlavor("I love the name of honor, more than I fear death.");
    }

    public static void main(String[] args) throws IOException {
        new Deck(Stream.of(
//                king().withId("king1").withBackground(Images.load("cards/king.jpg")),
//                king().withId("king2").withBackground(Images.load("cards/king2.jpg")),
//                king().withId("king3").withBackground(Images.load("cards/king3.jpg")),
//                king().withId("king4").withBackground(Images.load("cards/king4.png")),
//                king().withId("king5").withBackground(Images.load("cards/king5.jpg")),
//                king().withId("king6").withBackground(Images.load("cards/king6.jpg")),
//                king().withId("king7").withBackground(Images.load("cards/king7.jpg")),
                king().withId("king8").withBackground(Images.load("cards/king8.jpg")),

//                queen().withId("queen1").withBackground(Images.load("cards/queen.jpg")),
//                queen().withId("queen2").withBackground(Images.load("cards/queen2.jpg")),
//                queen().withId("queen3").withBackground(Images.load("cards/queen3.jpg")),
//                queen().withId("queen4").withBackground(Images.load("cards/queen4.jpg")),
                queen().withId("queen5").withBackground(Images.load("cards/queen5.jpg")),

//                usurper().withId("usurper1").withBackground(Images.load("cards/usurper.jpg")),
                usurper().withId("usurper2").withBackground(Images.load("cards/usurper2.jpg")),

//                assassin().withId("assassin1").withBackground(Images.load("cards/assassin.png")),
                assassin().withId("assassin2").withBackground(Images.load("cards/assassin2.jpg")),
//                assassin().withId("assassin3").withBackground(Images.load("cards/assassin3.jpg")),
                assassin().withId("assassin4").withBackground(Images.load("cards/assassin4.jpg")),
//                assassin().withId("assassin5").withBackground(Images.load("cards/assassin5.jpg")),

//                traitor().withId("traitor1").withBackground(Images.load("cards/traitor.png")),
                traitor().withId("traitor2").withBackground(Images.load("cards/traitor2.jpg")),

                guard().withId("guard1").withBackground(Images.load("cards/guard.jpg")),
//                guard().withId("guard2").withBackground(Images.load("cards/guard2.jpg")),
//                guard().withId("guard3").withBackground(Images.load("cards/guard3.jpg")),
//                guard().withId("guard4").withBackground(Images.load("cards/guard4.jpg")),
                guard().withId("guard5").withBackground(Images.load("cards/guard5.jpeg"))
//                guard().withId("guard6").withBackground(Images.load("cards/guard6.jpg"))
        )
                .map((b) -> b.withFrame(new SimpleProxyFrame()))
                .map(Proxy.Builder::build)
                .peek((p) -> {
                    try {
                        String id = p.getElements().get(TextElement.class, "id").getText();
                        System.out.println("Generate: "+id);
                        ImageIO.write(p.getImage(),
                                "png",
                                new File("proxies/images",
                                        id + ".png"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                })
                .map(Proxy::getCard))
                .generatePDF("tokens.pdf");
    }

}
