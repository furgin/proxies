package au.com.angry.proxy.elements;

import au.com.angry.proxy.ProxyElement;

import java.awt.image.BufferedImage;

public class ImageElement implements ProxyElement {
    private final BufferedImage image;

    public ImageElement(BufferedImage image) {
        this.image = image;
    }

    public BufferedImage getImage() {
        return image;
    }
}
