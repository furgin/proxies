package au.com.angry.proxy;

import au.com.angry.proxy.decks.Http;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.codec.digest.DigestUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MagicCard {

    private final String name;
    private JsonObject object;
    private List<MagicCard> prints;
    private List<BufferedImage> images;
    private MagicSet set;

    public MagicCard(String name) {
        this.name = name;
    }

    public MagicCard(JsonObject object) {
        this.name = object.get("name").getAsString();
        this.object = object;
    }

    public MagicCard(BufferedImage image) {
        this.name = "";
        this.images = Collections.singletonList(image);
    }

    public String getType() {
        JsonElement typeLine = getObject().get("type_line");
        if (typeLine != null) {
            return typeLine.getAsString();
        } else {
            return "";
        }
    }

    public List<MagicCard> getPrintings() {
        if (this.prints == null) {
            JsonObject loaded = getObject();
            if (loaded.has("prints_search_uri")) {
                String url = loaded.get("prints_search_uri").getAsString();
                JsonObject list = Http.load(url).getAsJsonObject();
                List<JsonObject> printings = new ArrayList<>();
                boolean done = false;
                while (!done) {
                    List<JsonObject> data = StreamSupport
                            .stream(list.get("data").getAsJsonArray().spliterator(), false)
                            .map(JsonElement::getAsJsonObject)
                            .collect(Collectors.toList());
                    printings.addAll(data);
                    if (list.get("has_more").getAsBoolean()) {
                        list = Http.load(list.get("next_page").getAsString()).getAsJsonObject();
                    } else {
                        done = true;
                    }
                }
                this.prints = printings.stream()
                        .map(MagicCard::new)
                        .collect(Collectors.toList());
            } else {
                throw new RuntimeException("no prints_search_uri");
            }
        }
        return this.prints;
    }

    private JsonObject getObject() {
        if (this.object == null) {
            try {
                this.object = Http.load("https://api.scryfall.com/cards/named?fuzzy=" + URLEncoder.encode(name, "UTF-8")).getAsJsonObject();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return this.object;
    }

    public List<BufferedImage> getImages() throws IOException {

        if (images != null) {
            return images;
        }

        JsonObject card = getObject();

        JsonObject imageUris = card.getAsJsonObject("image_uris");
        List<URL> imageUrls;
        if (imageUris == null) {
            imageUrls = StreamSupport
                    .stream(card.getAsJsonArray("card_faces").spliterator(), false)
                    .map(je -> je.getAsJsonObject().getAsJsonObject("image_uris").get("png").getAsString())
                    .map((s) -> {
                        try {
                            return new URL(s);
                        } catch (MalformedURLException e) {
                            return null;
                        }
                    })
                    .collect(Collectors.toList());
        } else {
            imageUrls = Collections.singletonList(new URL(imageUris.get("png").getAsString()));
        }

        List<BufferedImage> images = imageUrls.stream()
                .map((url) -> {
                    String id = DigestUtils.md2Hex(url.toString());

                    BufferedImage image = null;
                    File cachedImage = new File(Cache.getDirectory(), id + ".jpg");
                    try {
                        if (!cachedImage.exists()) {
                            image = Images.load(url);
                            ImageIO.write(image, "jpg", cachedImage);
                        } else {
                            image = ImageIO.read(cachedImage);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return image;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        this.images = images;
        return images;
    }

    public String getName() {
        return name;
    }

    public Date getSetReleaseDate() {
        return getSet().getReleasedDate();
    }

    public MagicSet getSet() {
        if(this.set==null) {
            this.set = new MagicSet(getObject().get("set").getAsString());
        }
        return this.set;
    }

    public String getFrame() {
        return getObject().get("frame").getAsString();
    }

    public boolean isFullArt() {
        return getObject().get("full_art").getAsBoolean();
    }

    public String getBorderColor() {
        return getObject().get("border_color").getAsString();
    }
}
