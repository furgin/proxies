package au.com.angry.proxy;

import java.io.File;

public class Cache {
    private static String directory = "cache";

    public static String getDirectory() {
        File dir = new File(directory);
        if(!dir.exists()) {
            dir.mkdirs();
        }
        return directory;
    }

    public static void setDirectory(String directory) {
        Cache.directory = directory;
    }
}
