package au.com.angry.proxy.image;

import au.com.angry.proxy.text.Utils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.util.ArrayList;
import java.util.List;

public class LayeredImage {
    private final List<Layer> layers = new ArrayList<>();
    private int width;
    private int height;

    public LayeredImage(int width, int height, List<Layer> layers) {
        this.width = width;
        this.height = height;
        this.layers.addAll(layers);
    }

    public BufferedImage getImage() {
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = Utils.getGraphics2D(bufferedImage);
        for (Layer layer : layers) {
            layer.draw(g2);
        }
        return bufferedImage;
    }

    public static class Builder {
        private final int width;
        private final int height;
        private final List<Layer> layers = new ArrayList<>();

        public Builder(int width, int height) {
            this.width = width;
            this.height = height;
        }

        public Builder withLayer(BufferedImage image) {
            layers.add(new ImageLayer(0,0,image));
            return this;
        }

        public Builder withLayer(int x, int y, BufferedImage image) {
            layers.add(new ImageLayer(x,y,image));
            return this;
        }

        public LayeredImage build() {
            return new LayeredImage(width,height,layers);
        }
    }
}
