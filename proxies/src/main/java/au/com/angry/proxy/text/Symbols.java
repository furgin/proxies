package au.com.angry.proxy.text;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Symbols {

    public static final Tile NUM_0 = new Tile(0,0);
    public static final Tile NUM_1 = new Tile(1,0);
    public static final Tile NUM_2 = new Tile(2,0);
    public static final Tile NUM_3 = new Tile(3,0);
    public static final Tile NUM_4 = new Tile(4,0);
    public static final Tile NUM_5 = new Tile(5,0);
    public static final Tile NUM_6 = new Tile(6,0);
    public static final Tile NUM_7 = new Tile(7,0);
    public static final Tile NUM_8 = new Tile(8,0);
    public static final Tile NUM_9 = new Tile(9,0);

    public static final Tile NUM_10 = new Tile(0,1);
    public static final Tile NUM_11 = new Tile(1,1);
    public static final Tile NUM_12 = new Tile(2,1);
    public static final Tile NUM_13 = new Tile(3,1);
    public static final Tile NUM_14 = new Tile(4,1);
    public static final Tile NUM_15 = new Tile(5,1);
    public static final Tile NUM_16 = new Tile(6,1);
    public static final Tile NUM_17 = new Tile(7,1);
    public static final Tile NUM_18 = new Tile(8,1);
    public static final Tile NUM_19 = new Tile(9,1);


    public static final Tile NUM_WU = new Tile(0,3);
    public static final Tile NUM_WB = new Tile(1,3);
    public static final Tile NUM_UB = new Tile(2,3);
    public static final Tile NUM_UR = new Tile(3,3);
    public static final Tile NUM_BR = new Tile(4,3);
    public static final Tile NUM_BG = new Tile(5,3);
    public static final Tile NUM_RW = new Tile(6,3);
    public static final Tile NUM_RG = new Tile(7,3);
    public static final Tile NUM_GW = new Tile(8,3);
    public static final Tile NUM_GU = new Tile(9,3);

    public static final Tile NUM_20 = new Tile(0,2);
    public static final Tile NUM_X = new Tile(1,2);
    public static final Tile NUM_Y = new Tile(2,2);
    public static final Tile NUM_Z = new Tile(3,2);
    public static final Tile COL_WHITE = new Tile(4,2);
    public static final Tile COL_BLUE = new Tile(5,2);
    public static final Tile COL_BLACK = new Tile(6,2);
    public static final Tile COL_RED = new Tile(7,2);
    public static final Tile COL_GREEN = new Tile(8,2);

    public static final Tile CUSTOM_SYMBOL = new Tile(0,0);

    public static final Tile TAP = new Tile(0,5);

    private static final Map<String,Tile> mapping = new HashMap<String,Tile>();

    private static final BufferedImage customSymbol;
    private static final BufferedImage symbols;
    static {
        try {
            customSymbol = ImageIO.read(Symbols.class.getClassLoader().getResourceAsStream("symbol.png"));
            symbols = ImageIO.read(Symbols.class.getClassLoader().getResourceAsStream("Mana.png"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        mapping.put("0",NUM_0);
        mapping.put("1",NUM_1);
        mapping.put("2",NUM_2);
        mapping.put("3",NUM_3);
        mapping.put("4",NUM_4);
        mapping.put("5",NUM_5);
        mapping.put("6",NUM_6);
        mapping.put("7",NUM_7);
        mapping.put("8",NUM_8);
        mapping.put("9",NUM_9);

        mapping.put("10",NUM_10);
        mapping.put("11",NUM_11);
        mapping.put("12",NUM_12);
        mapping.put("13",NUM_13);
        mapping.put("14",NUM_14);
        mapping.put("15",NUM_15);
        mapping.put("16",NUM_16);
        mapping.put("17",NUM_17);
        mapping.put("18",NUM_18);
        mapping.put("19",NUM_19);

        mapping.put("w/u",NUM_WU);
        mapping.put("w/b",NUM_WB);
        mapping.put("u/b",NUM_UB);
        mapping.put("u/r",NUM_UR);
        mapping.put("b/r",NUM_BR);
        mapping.put("b/g",NUM_BG);
        mapping.put("r/w",NUM_RW);
        mapping.put("r/g",NUM_RG);
        mapping.put("g/w",NUM_GW);
        mapping.put("g/u",NUM_GU);

        mapping.put("t",TAP);
        mapping.put("x",NUM_X);

        mapping.put("w",COL_WHITE);
        mapping.put("u",COL_BLUE);
        mapping.put("b",COL_BLACK);
        mapping.put("r",COL_RED);
        mapping.put("g",COL_GREEN);

        mapping.put("c",CUSTOM_SYMBOL);
    }

    private static class Tile{
        public final int x;
        public final int y;
        private Tile(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    public static void main(String[] args) throws IOException {
        render("Mana.png", NUM_1, COL_BLACK, COL_BLACK);
    }

    public static void render(String fileName, Tile... tiles) throws IOException {
        BufferedImage img = render(tiles);
        ImageIO.write(img, "png", new File(fileName));
    }

    public static BufferedImage render(String symbol) {
        System.out.println(symbol);
        Tile tile = mapping.get(symbol.toLowerCase());
        if(tile==CUSTOM_SYMBOL) {
            return customSymbol;
        } else {
            return render(tile);
        }
    }

    public static BufferedImage render(Tile... tiles) {
        BufferedImage img = new BufferedImage(tiles.length*100,100,BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D) img.getGraphics();
        int x=0;
        for (Tile tile : tiles) {
            g.drawImage(symbols.getSubimage(tile.x*100,tile.y*100,100,100),x,0,null);
            x+=100;
        }
        return img;
    }
}
