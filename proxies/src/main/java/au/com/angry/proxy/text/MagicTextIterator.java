package au.com.angry.proxy.text;

import java.awt.*;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.image.BufferedImage;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;
import java.util.Hashtable;
import java.util.Iterator;

import static au.com.angry.proxy.text.Utils.getGraphics2D;

public class MagicTextIterator implements Iterable<MagicTextIterator.Element>
{

    public static final String SUSPEND_TEXT = "\\(Rather than cast this card from your hand, " +
            "you may pay.*and exile it with X time counters on it. At the beginning of your upkeep, " +
            "remove a time counter. When the last is removed, cast it without paying its mana cost. " +
            "It has haste.\\)";
    private final Color color;
    private final String text;

    public MagicTextIterator(String text, Color color)
    {
        this.color = color;
        while(text.contains("\n\n")){
            text = text.replaceAll("\n\n","\n");
        }
        text = text.replaceAll("Annihilator (\\d+)","Annihilator {$1}");
        text = text.replaceAll(SUSPEND_TEXT,"");
        this.text = text;
    }

    @Override
    public Iterator<Element> iterator()
    {
        return new Iterator<Element>()
        {
            boolean italics = false;
            private int index = 0;

            public boolean hasNext()
            {
                return index < text.length();
            }

            public Element next()
            {
                StringBuilder buf = new StringBuilder();
                boolean reading = true;
                boolean readingSymbol = false;
                boolean readingText = false;
                boolean foundNewLine = false;
                boolean newItalics = italics;
                while (reading && index < text.length())
                {
                    char ch = text.charAt(index);
                    if (readingText || readingSymbol)
                    {
                        String stopChars = "{}()\n";
                        boolean found = false;
                        for (int i = 0; i < stopChars.toCharArray().length; i++) {
                             if(stopChars.charAt(i)==ch) {
                                 found = true;
                                 break;
                             }
                        }
                        if(found) {
                            if(ch==')') { buf.append(ch); newItalics=false; index++; }
                            if(ch=='}') { index++; }
                            break;
                        } else {
                            buf.append(ch);
                        }
                    }
                    else
                    {
                        if (ch == '{') // start a symbol
                        {
                            readingSymbol = true;
                        }
                        else if (ch == '(') // set italic text on
                        {
                            buf.append(ch);
                            readingText = true;
                            italics = true;
                        }
                        else if (ch == '\n')
                        {
                            foundNewLine = true;
                            reading = false;
                        }
                        else
                        {
                            buf.append(ch);
                            readingText = true;
                        }
                    }

                    index++;
                }
                boolean oldItalics = italics;
                italics = newItalics;
                if (foundNewLine)
                    return new NewLineElement();

                if (readingSymbol)
                    return new SymbolElement(buf.toString());
                else
                    return new TextElement(oldItalics, buf.toString(), color);
            }

            public void remove()
            {
                new UnsupportedOperationException();
            }
        };
    }

    public static interface Element
    {
        TypeWriter write(TypeWriter typeWriter);
    }

    public static class NewLineElement implements Element
    {
        public TypeWriter write(TypeWriter typeWriter)
        {
            typeWriter.newLine(15);
            return typeWriter;
        }
    }

    public static class TextElement implements Element
    {
        private final boolean italics;
        private final String text;
        private final Color color;

        public TextElement(boolean italics, String text, Color color)
        {
            this.italics = italics;
            this.text = text;
            this.color = color;
        }

        public String getText()
        {
            return text;
        }

        @Override
        public String toString() {
            return "TextElement{italics=" + italics + ", text='" + text + '\'' + '}';
        }

        public TypeWriter write(TypeWriter typeWriter)
        {
            int width = typeWriter.getWidth();
//            int height = typeWriter.getFinishedY();

            BufferedImage tmpImg = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g = getGraphics2D(tmpImg);
            Font font = typeWriter.getFont();
            if(italics)
                font = font.deriveFont(Font.ITALIC);
            g.setFont(font);

            // used to wrap the text if needed
            Hashtable<TextAttribute, Object> map = new Hashtable<TextAttribute, Object>();
            map.put(TextAttribute.SIZE, font.getSize2D());
            map.put(TextAttribute.FONT, font);
            AttributedString string = new AttributedString(text, map);
            AttributedCharacterIterator iterator = string.getIterator();
            LineBreakMeasurer lineMeasurer = new LineBreakMeasurer(iterator, g.getFontRenderContext());

            int paragraphStart = iterator.getBeginIndex();
            int paragraphEnd = iterator.getEndIndex();
            float formatWidth = (float) width;

            lineMeasurer.setPosition(paragraphStart);
            while (lineMeasurer.getPosition() < paragraphEnd)
            {
                int startPos = lineMeasurer.getPosition();
                float maxPhraseWidth = formatWidth - typeWriter.getX();

                if(maxPhraseWidth<0) {
                    typeWriter.newLine();
                    continue;
                }

                lineMeasurer.nextLayout(maxPhraseWidth);
                int endPos = lineMeasurer.getPosition();

                String phrase = text.substring(startPos, endPos);


//                System.out.println("phrase: '"+phrase+"'");
//                if(StringUtils.isNotEmpty(phrase)) {

                    FontMetrics fontMetrics = g.getFontMetrics();
                    BufferedImage phraseImg = new BufferedImage(fontMetrics.stringWidth(phrase),
                            typeWriter.getLineHeight() + fontMetrics.getDescent(),
                            BufferedImage.TYPE_INT_ARGB);
                    Graphics2D pg = getGraphics2D(phraseImg);
                    pg.setColor(color);
                    pg.setFont(font);
                    pg.drawString(phrase, 0, fontMetrics.getAscent());

                    typeWriter.write(0, 0, phraseImg);

                    if(endPos!=paragraphEnd)
                        typeWriter.newLine();

//                    typeWriter.newLine();
//                } else {
//                    typeWriter.newLine((int)(-typeWriter.getLineHeight()*3f/4f));
//                }
            }

            return typeWriter;
        }

    }

    public static class SymbolElement implements Element
    {
        private final String symbol;

        public SymbolElement(String text)
        {
            this.symbol = text;
        }

        public String getSymbol()
        {
            return symbol;
        }

        @Override
        public TypeWriter write(TypeWriter typeWriter)
        {
            int width = typeWriter.getWidth();
            int lineHeight = typeWriter.getLineHeight();
            int s = 2;

            BufferedImage symbol = Symbols.render(this.symbol);

            // java sux
            BufferedImage tmpImg = new BufferedImage(1, 1, BufferedImage.TYPE_4BYTE_ABGR);
            Graphics2D tmpG = getGraphics2D(tmpImg);
            tmpG.setFont(typeWriter.getFont());
            FontMetrics fontMetrics1 = tmpG.getFontMetrics();

            float x = fontMetrics1.getAscent();// * 0.9f;
            int h = (s * 2) + (int) x;
            int w = (s * 2) + (int) (x * (float) symbol.getHeight() / symbol.getWidth());
            Image image = symbol.getScaledInstance(w, h, BufferedImage.SCALE_SMOOTH);
            BufferedImage img = new BufferedImage(w + (s * 2), lineHeight, BufferedImage.TYPE_INT_ARGB);
            Graphics g = img.getGraphics();

            g.setFont(typeWriter.getFont());
            g.drawImage(image, 0, 0, null);

            if (typeWriter.getX() + img.getWidth() > width)
                typeWriter.newLine();
            typeWriter.write(0f, s, img);

            return typeWriter;
        }

        public String toString()
        {
            return "SymbolElement{symbol='" + symbol + '\'' + '}';
        }
    }

}
