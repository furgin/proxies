package au.com.angry.proxy;

public class Decks {
    public static final PrintingResolver RESOLVER = new PrintingResolver.Builder()
            .sort(PrintingResolver.Sort.DATE_ASC)
            .ignore("lea")
            .frame("2003")
            .fullArt()
            .excludeTypes("funny")
            .borderColor("black")
            .build();

    public static void main(String[] args) {
//        sultaiControl();
//        reckoner();
//        temurControl();
//        ballTribal();
//        jund();
    }

//    private static void jund() {
//        new Deck.Builder()
//                .resolver(RESOLVER)
//                .load("https://www.mtggoldfish.com/deck/download/994203")
//                .build()
//                .generateReport()
//                .generatePDF("jund.pdf");
//    }

    private static void reckoner() {
        new Deck.Builder()
                .resolver(RESOLVER)
                .add(4, "Boros Reckoner")
                .add(3, "Blasphemous Act")
                .add(4, "Serum Visions")
                .add(2, "Dispel")
                .add(3, "Monastery Mentor")
                .add(3, "Young Pyromancer")
                .add(4, "Path to Exile")
                .add(4, "Jace, the Mind Sculptor")
                .add(4, "Lightning Bolt")
                .add(4, "Spreading Sees")
                .add(2, "Remand")
                .add(1, "Supreme Verdict")
                .add(1, "Arid Mesa")
                .add(3, "Celestial Colonnade")
                .add(4, "Flooded Strand")
                .add(1, "Glacial Fortress")
                .add(2, "Hallowed Fountain")
                .add(2, "Island")
                .add(1, "Mountain")
                .add(1, "Sacred Foundry")
                .add(3, "Scalding Tarn")
                .add(2, "Steam Vents")
                .add(1, "Sulfur Falls")
                .add(2, "Field of Ruin")
                .build()
                .generatePDF("boros-control.pdf")
                .generateReport();
    }

    private static void sultaiControl() {
        new Deck.Builder()
                .resolver(RESOLVER)
                .add(3, "Jace, the Mind Sculptor")
                .add(3, "Liliana of the Veil")
                .add(2, "Thoughtseize")
                .add(4, "Inquisition of Kozilek")
                .add(3, "Fatal Push")
                .add(4, "Snapcaster Mage")
                .add(1, "Damnation")
                .add(4, "Spreading Seas")
                .add(3, "Bitterblossom")
                .add(3, "Abrupt Decay")
                .add(1, "Maelstrom Pulse")
                .add(4, "Serum Visions")
                .add(3, "Watery Grave")
                .add(4, "Polluted Delta")
                .add(2, "Field of Ruin")
                .add(2, "Overgrown Tomb")
                .add(3, "Verdant Catacombs")
                .add(3, "Creeping Tar Pit")
                .add(2, "Island")
                .add(2, "Swamp")
                .add(2, "Forest")
                .add(2, "Darkslick Shores")
                .sideboard(2, "Surgical Extraction")
                .sideboard(2, "Damnation")
                .sideboard(2, "Engineered Explosives")
                .sideboard(2, "Ceremonious Rejection")
                .sideboard(1, "Maelstrom Pulse")
                .sideboard(2, "Collective Brutality")
                .sideboard(1, "Liliana's Defeat")
                .sideboard(2, "Liliana, the Last Hope")
                .sideboard(1, "Thoughtseize")
                .build()
                .generatePDF("sultai-control.pdf")
                .generateReport();
    }

    private static void temurControl() {
        new Deck.Builder()
                .resolver(RESOLVER)
                .add(3, "Jace, the Mind Sculptor")
                .add(3, "Tarmogoyf")
                .add(3, "Snapcaster Mage")
                .add(2, "Vendilion Clique")
                .add(2, "Huntmaster of the Fells")
                .add(4, "Ancestral Vision")
                .add(4, "Serum Visions")
                .add(1, "Roast")
                .add(4, "Lightning Bolt")
                .add(2, "Spell Snare")
                .add(2, "Mana Leak")
                .add(1, "Logic Knot")
                .add(1, "Harvest Pyre")
                .add(3, "Cryptic Command")
                .add(2, "Blood Moon")
                .add(1, "Spreading Seas")
                .add(1, "Forest")
                .add(1, "Mountain")
                .add(6, "Island")
                .add(1, "Breeding Pool")
                .add(1, "Stomping Ground")
                .add(2, "Steam Vents")
                .add(4, "Scalding Tarn")
                .add(4, "Misty Rainforest")
                .add(1, "Wooded Foothills")
                .add(1, "Sulfur Falls")
                .add(1, "Blood Moon")
                .add(3, "Ancient Grudge")
                .add(2, "Anger of the Gods")
                .add(1, "Dispel")
                .add(1, "Negate")
                .add(2, "Ceremonious Rejection")
                .add(1, "Keranos, God of Storms")
                .add(2, "Feed the Clan")
                .add(1, "Engineered Explosives")
                .add(1, "Crumble to Dust")
                .build()
                .generatePDF("temurControl.pdf")
                .generateReport();
    }

    private static void ballTribal() {
        new Deck.Builder()
                .resolver(RESOLVER)
                .add(4, "Birds of Paradise")
                .add(4, "Lightning Bolt")
                .add(3, "Strangleroot Geist")
                .add(4, "Hellspark Elemental")
                .add(4, "Hell's Thunder")
                .add(4, "Ball Lightning")
                .add(4, "Primal Forcemage")
                .add(4, "Groundbreaker")
                .add(4, "Bloodbraid Elf")
                .add(4, "Collected Company")
                .add(3, "Copperline Gorge")
                .add(2, "Fire-lit Thicket")
                .add(3, "Forest")
                .add(1, "Mountain")
                .add(3, "Misty Rainforest")
                .add(4, "Stomping Ground")
                .add(1, "Windswept Heath")
                .add(4, "Wooded Foothills")
                .sideboard(1, "Ancient Grudge")
                .sideboard(3, "Defense Grid")
                .sideboard(1, "Kozilek's Return")
                .sideboard(2, "Anger of the Gods")
                .sideboard(1, "Choke")
                .sideboard(2, "Reclamation Sage")
                .sideboard(3, "Wild Defiance")
                .sideboard(2, "Kitchen Finks")
                .build()
                .generatePDF("ballTribal.pdf")
                .generateReport();
    }
}
