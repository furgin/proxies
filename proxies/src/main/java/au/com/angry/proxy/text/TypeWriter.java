package au.com.angry.proxy.text;

import java.awt.*;
import java.awt.geom.GeneralPath;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.util.ArrayList;
import java.util.List;

import static au.com.angry.proxy.text.Utils.getGraphics2D;

public class TypeWriter {

    public static class Location {
        public final float x;
        public final float y;

        public Location(float x, float y) {
            this.x = x;
            this.y = y;
        }
    }

    public static class TypeWriterImage {
        public final Location location;
        public final BufferedImage image;

        public TypeWriterImage(Location location, BufferedImage image) {
            this.location = location;
            this.image = image;
        }
    }

    private final List<TypeWriterImage> images = new ArrayList<TypeWriterImage>();
    private final int width;
    private final int lineHeight;
    private final Font font;
    private final boolean shadow;
    private float x = 0;
    private float y = 0;
    private float maxWidth = 0;

    public TypeWriter(int width, int lineHeight, Font font, boolean shadow) {
        this.width = width;
        this.lineHeight = lineHeight;
        this.font = font;
        this.shadow = shadow;
        this.y = 0;
    }

    public void newLine(int extra) {
        x = 0;
        y += lineHeight + extra;
    }

    public void newLine() {
        newLine(0);
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public Font getFont() {
        return font;
    }

    public void write(float offsetX, float offsetY, BufferedImage image) {
        images.add(new TypeWriterImage(new Location(x + offsetX, y + offsetY), image));
        x += image.getWidth();
        maxWidth = Math.max(x, maxWidth);
    }

    public BufferedImage getImage() {
        GeneralPath shape = new GeneralPath();
        for (TypeWriterImage i : images) {
            BufferedImage img = i.image;
            Rectangle rect = new Rectangle((int) i.location.x, (int) i.location.y, img.getWidth(), img.getHeight());
//            System.out.println("add rectangle: "+rect);
            shape.append(rect, false);
        }
        Rectangle rect = shape.getBounds();
//        System.out.println("bounds: "+rect);

        float rad = 3f;
        BufferedImage bufferedImage = new BufferedImage((int) rect.getWidth() + (int) (rad * 2), (int) rect.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = getGraphics2D(bufferedImage);

        // parse 1 - symbols
        for (TypeWriterImage image : images) {
            g.drawImage(image.image, (int) (image.location.x), (int) (image.location.y), null);
        }

        // parse 2
        BufferedImage img = new BufferedImage((int) rect.getWidth() + 6, (int) rect.getHeight(), BufferedImage.TYPE_INT_ARGB);
//        BufferedImage shadowedImage = new BufferedImage((int) rect.getWidth() + 6, (int) rect.getFinishedY(), BufferedImage.TYPE_INT_ARGB);
//        ShadowFilter filter = new ShadowFilter(rad, -2f, -2f, .6f);
//        filter.setAddMargins(false);
//        filter.filter(bufferedImage, shadowedImage);
//        Filters.shadow(img);
//        img = dropShadow(img);
        g = getGraphics2D(img);

        // parse 3 - text
        for (TypeWriterImage image : images) {
            g.drawImage(image.image, (int) image.location.x, (int) image.location.y, null);
        }
//        System.out.println("image height: "+img.getFinishedY());

        return shadow ? dropShadow(img) : img;
    }

    public int getLineHeight() {
        return lineHeight;
    }

    private static BufferedImage dropShadow(BufferedImage img) {
        // a filter which converts all colors except 0 to black
        ImageProducer prod = new FilteredImageSource(img.getSource(),
                new RGBImageFilter() {
                    @Override
                    public int filterRGB(int x, int y, int rgb) {
                        if (rgb == 0)
                            return 0;
                        else
                            return 0x99000000;
                    }
                });
        // create whe black image
        Image shadow = Toolkit.getDefaultToolkit().createImage(prod);

        // result
        BufferedImage result = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
        Graphics2D g = getGraphics2D(result);

        // draw shadow with offset
        g.drawImage(shadow, 2, 2, null);
        // draw original image
        g.drawImage(img, 0, 0, null);

        return result;
    }

//    private static double distance(double x1, double y1, double x2, double y2) {
//        return Math.hypot(x1 - x2, y1 - y2);
//
//    }

//    private static BufferedImage border(BufferedImage img) {
//        int r = 3;
//
//        // a filter which converts all colors except 0 to black
//        ImageProducer prod = new FilteredImageSource(img.getSource(),
//                new RGBImageFilter() {
//                    private double ddd[][] = new double[img.getWidth()][img.getHeight()];
//
//                    @Override
//                    public int filterRGB(int x, int y, int rgb) {
//                        if (rgb == 0) {
//                            double d = r;
//                            if (ddd[x][y] == 0) {
//                                for (int a = x - r; a <= x + r; a++) {
//                                    for (int b = y - r; b <= y + r; b++) {
//                                        double dd = distance(a, b, x, y);
//                                        try {
//                                            if (img.getRGB(a, b) != 0 && dd < d) {
//                                                d = dd;
//                                            }
//                                        } catch (Exception e) {
//                                            // ignore
//                                        }
//                                    }
//                                }
//                                ddd[x][y] = d;
//                            } else {
//                                d = ddd[x][y];
//                            }
//                            return d < r ? 0xff000000 : 0;
//                        } else
//                            return 0xff000000;
//                    }
//                });
//        // create whe black image
//        Image shadow = Toolkit.getDefaultToolkit().createImage(prod);
//
//        // result
//        BufferedImage result = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
//        Graphics2D g = getGraphics2D(result);
//
//        // draw shadow with offset
//        g.drawImage(shadow, 0, 0, null);
//        // draw original image
//        g.drawImage(img, 0, 0, null);
//
//        return result;
//    }
}