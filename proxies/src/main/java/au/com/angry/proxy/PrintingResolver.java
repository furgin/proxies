package au.com.angry.proxy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static au.com.angry.proxy.PrintingResolver.Sort.NONE;

public class PrintingResolver {

    private String code;
    private Sort sort;
    private String frame;
    private List<String> ignore;
    private List<String> excludeTypes;
    private boolean fullArt;
    private String borderColor;

    public enum Sort {
        NONE,
        DATE_ASC,
        DATE_DESC
    }

    PrintingResolver(String code,
                     Sort sort,
                     String frame,
                     List<String> ignore,
                     List<String> excludeTypes,
                     boolean fullArt,
                     String borderColor) {
        this.code = code;
        this.sort = sort;
        this.frame = frame;
        this.ignore = ignore;
        this.excludeTypes = excludeTypes;
        this.fullArt = fullArt;
        this.borderColor = borderColor;
    }

    public MagicCard resolvePrinting(MagicCard card) {
        List<MagicCard> printings = card.getPrintings();

        if(code!=null) {
            for (MagicCard printing : printings) {
                if(printing.getSet().getCode().equals(code)) {
                    return printing;
                }
            }
        }

        if (sort != NONE) {
            printings = printings
                    .stream()
                    .filter((c) -> c.getSet().hasReleasedDate())
                    .collect(Collectors.toList());
        }

        Comparator<MagicCard> comparator = (a, b) -> {
            switch (sort) {
                case DATE_ASC:
                    return a.getSetReleaseDate().compareTo(b.getSetReleaseDate());
                case DATE_DESC:
                    return b.getSetReleaseDate().compareTo(a.getSetReleaseDate());
            }
            return 0;
        };

        printings = printings
                .stream()
                .sorted(comparator)
                .filter((c) -> !ignore.contains(c.getSet().getCode()))
                .filter((c) -> !excludeTypes.contains(c.getSet().getSetType()))
                .collect(Collectors.toList());

        int score = -1;
        MagicCard thisPrinting = null;

        for (MagicCard printing : printings) {
            int thisScore = score(printing);
            if (thisScore > score) {
                score = thisScore;
                thisPrinting = printing;
            }
        }

        return thisPrinting;
    }

    private int score(MagicCard card) {
        int score = 0;
        if (fullArt && card.isFullArt()) score += 2;
        if (frame != null && card.getFrame().equals(frame)) score++;
        if (borderColor != null && card.getBorderColor().equals(borderColor)) score++;
        return score;
    }

    public static class Builder {
        private Sort sort = NONE;
        private String frame = null;
        private final List<String> ignore = new ArrayList<>();
        private final List<String> excludeTypes = new ArrayList<>();
        private boolean fullArt = false;
        private String code = null;
        private String borderColor = null;

        public PrintingResolver build() {
            return new PrintingResolver(code, sort, frame, ignore, excludeTypes, fullArt, borderColor);
        }

        public PrintingResolver.Builder sort(Sort sort) {
            this.sort = sort;
            return this;
        }

        public Builder frame(String frame) {
            this.frame = frame;
            return this;
        }

        public Builder ignore(String... code) {
            this.ignore.addAll(Arrays.asList(code));
            return this;
        }

        public Builder fullArt() {
            this.fullArt = true;
            return this;
        }

        public Builder excludeTypes(String... types) {
            this.excludeTypes.addAll(Arrays.asList(types));
            return this;
        }

        public Builder setCode(String code) {
            this.code = code;
            return this;
        }

        public Builder borderColor(String borderColor) {
            this.borderColor = borderColor;
            return this;
        }
    }
}
