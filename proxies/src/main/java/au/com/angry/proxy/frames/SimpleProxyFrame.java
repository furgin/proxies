package au.com.angry.proxy.frames;

import au.com.angry.proxy.Frame;
import au.com.angry.proxy.Images;
import au.com.angry.proxy.elements.Elements;
import au.com.angry.proxy.elements.ImageElement;
import au.com.angry.proxy.elements.TextElement;
import au.com.angry.proxy.image.LayeredImage;
import au.com.angry.proxy.text.Fonts;
import au.com.angry.proxy.text.TextRendering;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.awt.image.RenderedImage;

import static au.com.angry.proxy.text.Utils.getGraphics2D;

public class SimpleProxyFrame implements Frame {

    public static final int WIDTH = 791;
    public static final int HEIGHT = 1107;

    @Override
    public BufferedImage render(Elements elements) {

        BufferedImage background = Images.width(WIDTH - 80, elements.get(ImageElement.class, "background").getImage());
        background = Images.height(HEIGHT - 80, background);

        int minWidth = WIDTH - 80;
        if (background.getWidth() < minWidth) {
            background = Images.width(minWidth, background);
        }

        int minHeight = HEIGHT - 80;
        if (background.getHeight() < minHeight) {
            background = Images.height(minHeight, background);
        }

        return new LayeredImage.Builder(WIDTH, HEIGHT)
                .withLayer(WIDTH / 2 - background.getWidth() / 2,
                        HEIGHT / 2 - background.getHeight() / 2, background)
                .withLayer(Images.load("border.png"))
                .withLayer(40, 40, Images.load("simple/inner-frame.png"))
                .withLayer(textLayer(elements))
                .withLayer(nameLayer(elements))
                .build()
                .getImage();
    }

    private BufferedImage nameLayer(Elements elements) {
        BufferedImage img = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = getGraphics2D(img);

        BufferedImage frameImg = Images.load("simple/name-frame.png");
        g2.drawImage(frameImg, WIDTH / 2 - frameImg.getWidth() / 2, 50 - frameImg.getHeight() / 2, null);

        TextElement name = elements.get(TextElement.class, "name");

        TextRendering nameRendering = new TextRendering(Fonts.FONT_MPLANTI1.deriveFont(48f), name.getText(), WIDTH, Color.black, false);
        boolean done = false;
        while (!done) {
            BufferedImage nameImage = nameRendering.render();
            if (nameImage.getWidth() < 180) {
                g2.drawImage(nameImage, WIDTH / 2 - nameImage.getWidth() / 2, 50 - nameImage.getHeight() / 2, null);
                done = true;
            } else {
                nameRendering = nameRendering.smaller();
            }
        }
        return img;
    }

    private BufferedImage textLayer(Elements elements) {
        BufferedImage img = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);

        TextElement rules = elements.get(TextElement.class, "rules");
        TextElement flavor = elements.get(TextElement.class, "flavor");
        TextElement type = elements.get(TextElement.class, "type");

        TextRendering rulesRendering = new TextRendering(Fonts.FONT_MPLANTI1.deriveFont(36f), rules.getText(), WIDTH - 160, Color.white, true);
        TextRendering flavorRendering = new TextRendering(Fonts.FONT_MPLANTINI.deriveFont(32f), flavor.getText(), WIDTH - 160, Color.white, true);
        TextRendering typeRendering = new TextRendering(Fonts.FONT_MPLANTI1.deriveFont(46f), type.getText(), WIDTH - 160, Color.white, true);

        Graphics2D g2 = getGraphics2D(img);
        BufferedImage typeImage = typeRendering.render();

        boolean done = false;
        while (!done) {
            BufferedImage rulesImage = rulesRendering.render();
            BufferedImage flavorImage = flavorRendering.render();

            int renderedHeight = rulesImage.getHeight() + flavorImage.getHeight() + typeImage.getHeight() + 60;

            if (renderedHeight < (HEIGHT / 2) - 160) {
                done = true;

//                g2.setColor(new Color(0, 0, 0, 125));
//                g2.fillRect(60, HEIGHT - 140 - renderedHeight, WIDTH - 125, renderedHeight + 20);

                int y = HEIGHT - 140;
                g2.drawImage(flavorImage, 80, y - flavorImage.getHeight(), null);
                y = y - flavorImage.getHeight() - 20;
                g2.drawImage(rulesImage, 80, y - rulesImage.getHeight(), null);
                y = y - rulesImage.getHeight() - 20;
                g2.drawImage(typeImage, 80, y - typeImage.getHeight(), null);
                y = y - typeImage.getHeight() - 20;
                System.out.println(y);
            } else {
                rulesRendering = rulesRendering.smaller();
                flavorRendering = flavorRendering.smaller();
            }
        }

//        g2.setColor(new Color(0, 0, 0, 125));
//        g2.fillRect(60, HEIGHT - 140 - renderedHeight, WIDTH - 125, renderedHeight + 20);
//
//        int y = HEIGHT - 140;
//        g2.drawImage(flavorImage, 80, y - flavorImage.getHeight(), null);
//        y = y - flavorImage.getHeight() - 20;
//        g2.drawImage(rulesImage, 80, y - rulesImage.getHeight(), null);
//        y = y - rulesImage.getHeight() - 20;
//        g2.drawImage(typeImage, 80, y - typeImage.getHeight(), null);
//        y = y - typeImage.getHeight() - 20;
//        System.out.println(y);

        return img; //border(img);
    }

    private static double distance(double x1, double y1, double x2, double y2) {
        return Math.hypot(x1 - x2, y1 - y2);

    }

    private static BufferedImage border(BufferedImage img) {
        int r = 3;

        // a filter which converts all colors except 0 to black
        ImageProducer prod = new FilteredImageSource(img.getSource(),
                new RGBImageFilter() {
                    private double ddd[][] = new double[img.getWidth()][img.getHeight()];

                    @Override
                    public int filterRGB(int x, int y, int rgb) {
                        if (rgb == 0) {
                            double d = r;
                            if (ddd[x][y] == 0) {
                                for (int a = x - r; a <= x + r; a++) {
                                    for (int b = y - r; b <= y + r; b++) {
                                        double dd = distance(a, b, x, y);
                                        try {
                                            if (img.getRGB(a, b) != 0 && dd < d) {
                                                d = dd;
                                            }
                                        } catch (Exception e) {
                                            // ignore
                                        }
                                    }
                                }
                                ddd[x][y] = d;
                            } else {
                                d = ddd[x][y];
                            }
                            return d < r ? 0xff000000 : 0;
                        } else
                            return 0xff000000;
                    }
                });
        // create whe black image
        Image shadow = Toolkit.getDefaultToolkit().createImage(prod);

        // result
        BufferedImage result = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
        Graphics2D g = getGraphics2D(result);

        // draw shadow with offset
        g.drawImage(shadow, 0, 0, null);
        // draw original image
        g.drawImage(img, 0, 0, null);

        return result;
    }

}
