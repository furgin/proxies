package au.com.angry.proxy.image;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageLayer implements Layer {
    private final int x;
    private final int y;
    private final BufferedImage image;

    ImageLayer(int x, int y, BufferedImage image) {
        this.x = x;
        this.y = y;
        this.image = image;
    }

    @Override
    public void draw(Graphics2D g2) {
        g2.drawImage(image, x, y, null);
    }
}
