package au.com.angry.proxy.text;

import org.apache.commons.codec.binary.Base64;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by mjensen on 23/06/2014.
 */
public class Utils {

    public static Graphics2D getGraphics2D(BufferedImage img)
    {
        Graphics2D g = (Graphics2D) img.getGraphics();
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
//        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        return g;
    }

    public static String dataUrl(BufferedImage img) {
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        try {
            ImageIO.write(img,"png",buf);
            return "data:image/png;base64,"+Base64.encodeBase64String(buf.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Utils() {
    }

    public static BufferedImage buffer(Image image) {
        BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        getGraphics2D(bufferedImage).drawImage(image,0,0,null);
        return bufferedImage;
    }

    public static interface Callback<A>
    {
        void call(A a);
    }

    public static void draw(BufferedImage img, Callback<Graphics2D> callback)
    {
        Graphics2D g = getGraphics2D(img);
        try
        {
            callback.call(g);
        }
        finally
        {
            g.dispose();
        }
    }
}
