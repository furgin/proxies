package au.com.angry.proxy;

import au.com.angry.proxy.decks.MWSReader;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Utilities;
import com.itextpdf.text.pdf.PdfWriter;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Deck {

    private static class NormalizedCard {
        final int count;
        final MagicCard magicCard;

        private NormalizedCard(int count, MagicCard magicCard) {
            this.count = count;
            this.magicCard = magicCard;
        }
    }

    private final List<MagicCard> magicCards = new ArrayList<>();
    private final List<MagicCard> sideboard = new ArrayList<>();

    public Deck(List<MagicCard> magicCards, List<MagicCard> sideboard) {
        this.magicCards.addAll(magicCards);
        this.sideboard.addAll(sideboard);
    }

    public Deck(Stream<MagicCard> stream) {
        Collections.addAll(this.magicCards, stream.toArray(MagicCard[]::new));
    }

    public Deck generatePDF(String filename) {
        generatePDF(new File(filename));
        return this;
    }

    public File generatePDF(File file) {

        float g = 1f;
        float cardWidth = 63f + g * 2f;
        float cardHeight = 88f + g * 2f;

        float rxs[] = {
                7.5f - g,
                7.5f + 63f + 3f - g,
                7.5f + 63f + 3f + 63f + 3f - g
        };

        float rys[] = {
                13.5f - g,
                13.5f + 88f + 3f - g,
                13.5f + 88f + 3f + 88f + 3f - g
        };

        float xs[] = {
                Utilities.millimetersToPoints(rxs[0]),
                Utilities.millimetersToPoints(rxs[1]),
                Utilities.millimetersToPoints(rxs[2]),
        };
        float ys[] = {
                Utilities.millimetersToPoints(rys[0]),
                Utilities.millimetersToPoints(rys[1]),
                Utilities.millimetersToPoints(rys[2]),
        };
        float locations[][] = {
                {xs[0], ys[2]},
                {xs[1], ys[2]},
                {xs[2], ys[2]},
                {xs[0], ys[1]},
                {xs[1], ys[1]},
                {xs[2], ys[1]},
                {xs[0], ys[0]},
                {xs[1], ys[0]},
                {xs[2], ys[0]},
        };
        float rlocations[][] = {
                {rxs[0], rys[2]},
                {rxs[1], rys[2]},
                {rxs[2], rys[2]},
                {rxs[0], rys[1]},
                {rxs[1], rys[1]},
                {rxs[2], rys[1]},
                {rxs[0], rys[0]},
                {rxs[1], rys[0]},
                {rxs[2], rys[0]},
        };

        float width = Utilities.millimetersToPoints(cardWidth);
        float height = Utilities.millimetersToPoints(cardHeight);

        try {
            Document doc = new Document(PageSize.A4, 0, 0, 0, 0);
            PdfWriter.getInstance(doc, new FileOutputStream(file));
            try {
                doc.open();
                int location = 0;

                List<MagicCard> allMagicCards = new ArrayList<>();
                allMagicCards.addAll(magicCards);
                allMagicCards.addAll(sideboard);

                for (MagicCard magicCard : allMagicCards) {

                    List<BufferedImage> images = magicCard.getImages();
                    for (BufferedImage img : images) {

                        // pdf
                        ByteArrayOutputStream output = new ByteArrayOutputStream();
                        ImageIO.write(img, "png", output);
                        Image image = Image.getInstance(output.toByteArray());

                        image.setCompressionLevel(0);
//                        image.setDpi(2400, 2400);
                        image.setAbsolutePosition(locations[location][0], locations[location][1]);
                        image.scaleAbsolute(width, height);

                        doc.add(image);

                        location++;
                        if (location >= 9) {
                            // create a new page
                            doc.newPage();
                            location = 0;
                        }
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();

            } finally {
                try {
                    doc.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (DocumentException | FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        return file;
    }

    private BufferedImage resize(BufferedImage bufferedImage, int imageWidth, int imageHeight) {
        BufferedImage scaledImage = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
        scaledImage.getGraphics().drawImage(bufferedImage, 0, 0, imageWidth, imageHeight, null);
        return scaledImage;
    }

    private BufferedImage copy(BufferedImage bufferedImage) {
        return resize(bufferedImage, bufferedImage.getWidth(), bufferedImage.getHeight());
    }

    private static Map<String, NormalizedCard> normalize(List<MagicCard> magicCards) {
        Map<String, NormalizedCard> normalized = new HashMap<>();
        for (MagicCard magicCard : magicCards) {
            if (!normalized.containsKey(magicCard.getName())) {
                normalized.put(magicCard.getName(), new NormalizedCard(1, magicCard));
            } else {
                NormalizedCard c = normalized.get(magicCard.getName());
                normalized.put(magicCard.getName(), new NormalizedCard(c.count + 1, magicCard));
            }
        }
        return normalized;
    }

    private static String group(String typeLine) {
        if (typeLine.startsWith("Basic Land") || typeLine.startsWith("Land")) {
            return "Lands";
        }
        if (typeLine.startsWith("Instant") || typeLine.startsWith("Sorcery")) {
            return "Spells";
        }
        if (typeLine.contains("Planeswalker")) {
            return "Planeswalkers";
        }
        if (typeLine.startsWith("Creature")) {
            return "Creatures";
        }
        if (typeLine.contains("Enchantment")) {
            return "Enchantments";
        }
        return typeLine;
    }

    public Deck generateReport() {

        int mainCount = 0;
        Map<String, NormalizedCard> normalizedMainDeck = normalize(this.magicCards);
        Map<String, NormalizedCard> normalizedSideboard = normalize(this.sideboard);

        StringBuilder mainBuffer = new StringBuilder();

        List<String> types = this.magicCards.stream().map(MagicCard::getType).map(Deck::group).sorted().distinct().collect(Collectors.toList());
        for (String type : types) {

            StringBuilder buf = new StringBuilder();
            int count = 0;
            for (Map.Entry<String, NormalizedCard> e : normalizedMainDeck.entrySet()) {
                NormalizedCard card = e.getValue();
                if (group(card.magicCard.getType()).equals(type)) {
                    count += card.count;
                    mainCount += card.count;
                    buf.append(String.format("%d [%s] %s", card.count, card.magicCard.getSet().getCode(), card.magicCard.getName())).append("\n");
                }
            }

            mainBuffer.append(String.format("// %s (%d)", type, count)).append("\n");
            mainBuffer.append(buf.toString()).append("\n");
        }

        System.out.println(String.format("// Main (%d)", mainCount));
        System.out.println();
        System.out.println(mainBuffer.toString());

        int sbCount = 0;
        StringBuilder sbBuffer = new StringBuilder();
        for (Map.Entry<String, NormalizedCard> e : normalizedSideboard.entrySet()) {
            NormalizedCard card = e.getValue();
            sbBuffer.append(String.format("%d [%s] %s", card.count, card.magicCard.getSet().getCode(), card.magicCard.getName())).append("\n");
            sbCount += card.count;
        }

        System.out.println(String.format("// Sideboard (%d)", sbCount));
        System.out.println(sbBuffer.toString());

        return this;
    }

    public static class Builder {

        private class ResolvingCard {
            private final MagicCard card;
            private final PrintingResolver resolver;

            private ResolvingCard(MagicCard card, PrintingResolver resolver) {
                this.card = card;
                this.resolver = resolver;
            }

            MagicCard resolve(PrintingResolver resolver) {
                if(this.resolver!=null) {
                    return this.resolver.resolvePrinting(this.card);
                } else {
                    return resolver.resolvePrinting(this.card);
                }
            }
        }

        private final List<ResolvingCard> cards = new ArrayList<>();
        private final List<ResolvingCard> sideboard = new ArrayList<>();
        private PrintingResolver printingResolver = new PrintingResolver.Builder().build();

        public Deck build() {
            return new Deck(
                    cards.stream()
                            .map((rc) -> rc.resolve(printingResolver))
                            .collect(Collectors.toList()),
                    sideboard.stream()
                            .map((rc) -> rc.resolve(printingResolver))
                            .collect(Collectors.toList()));
        }

        public Builder load(String url) {
            try {
                new MWSReader().read(new URL(url).openStream(),this);
//                cards.addAll(
//
//                        new MWSReader()
//                                .read(new URL(url).openStream())
//                                .stream()
//                                .map((c) -> new ResolvingCard(c, null))
//                                .collect(Collectors.toList())
//                );
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return this;
        }

        public Builder add(int count, String name) {
            for (int i = 0; i < count; i++) {
                cards.add(new ResolvingCard(new MagicCard(name), null));
            }
            return this;
        }

        public Builder add(int count, String set, String name) {
            PrintingResolver resolver = new PrintingResolver.Builder().setCode(set).build();
            for (int i = 0; i < count; i++) {
                this.cards.add(new ResolvingCard(new MagicCard(name), resolver));
            }
            return this;
        }

        public Builder sideboard(int count, String name) {
            for (int i = 0; i < count; i++) {
                sideboard.add(new ResolvingCard(new MagicCard(name), null));
            }
            return this;
        }

        public Builder sideboard(int count, String set, String name) {
            PrintingResolver resolver = new PrintingResolver.Builder().setCode(set).build();
            for (int i = 0; i < count; i++) {
                this.sideboard.add(new ResolvingCard(new MagicCard(name), resolver));
            }
            return this;
        }

        public Builder resolver(PrintingResolver printingResolver) {
            this.printingResolver = printingResolver;
            return this;
        }
    }
}
