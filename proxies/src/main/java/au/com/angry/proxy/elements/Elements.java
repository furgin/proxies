package au.com.angry.proxy.elements;

import au.com.angry.proxy.ProxyElement;

import java.util.HashMap;
import java.util.Map;

public class Elements {
    private final Map<String,ProxyElement> elements = new HashMap<>();

    public void put(String name, ProxyElement element) {
        elements.put(name,element);
    }

    public <T> T get(Class<T> type, String key) {
        //noinspection unchecked
        return (T)elements.get(key);
    }
}
