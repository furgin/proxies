package au.com.angry.proxy;

import au.com.angry.proxy.text.Utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class Images {

    public static BufferedImage load(URL url) {
        System.out.println("downloading: " + url);
        try (InputStream in = url.openStream()){
            return ImageIO.read(in);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public static BufferedImage load(String resource) {
        try {
            return ImageIO.read(Images.class.getClassLoader().getResourceAsStream("images/" + resource));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static BufferedImage width(int width, BufferedImage img) {
        int height = (int) (((float) width) / img.getWidth() * img.getHeight());
        BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = Utils.getGraphics2D(newImage);
        g2.drawImage(img, 0, 0, width, height, null);
        return newImage;
    }

    public static BufferedImage height(int height, BufferedImage img) {
        int width = (int) (((float) height) / img.getHeight() * img.getWidth());
        BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = Utils.getGraphics2D(newImage);
        g2.drawImage(img, 0, 0, width, height, null);
        return newImage;
    }
}
