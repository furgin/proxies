package au.com.angry.proxy;

import au.com.angry.proxy.decks.Http;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MagicSet {
    private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private String code;
    private JsonObject object;

    public MagicSet(String code) {
        this.code = code;
    }

    private JsonObject getObject() {
        if (this.object == null) {
            try {
                this.object = Http.load(String.format("https://api.scryfall.com/sets/%s",
                        URLEncoder.encode(code,"UTF-8"))).getAsJsonObject();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return this.object;
    }

    public Date getReleasedDate() {
        try {
            return dateFormat.parse(getObject().get("released_at").getAsString());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

    }

    public boolean hasReleasedDate() {
        return getObject().has("released_at");
    }

    public String getCode() {
        return code;
    }

    public String getSetType() {
        return getObject().has("set_type") ? getObject().get("set_type").getAsString() : "unknown";
    }
}
