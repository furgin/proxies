package au.com.angry.proxy.image;

import java.awt.*;

public interface Layer {
    void draw(Graphics2D g2);
}
