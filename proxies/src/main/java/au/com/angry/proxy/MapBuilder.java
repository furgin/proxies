package au.com.angry.proxy;

import java.util.HashMap;
import java.util.Map;

public class MapBuilder<K, V> {

    private Map<K, V> data = new HashMap<>();

    public MapBuilder<K, V> with(K key, V value) {
        data.put(key, value);
        return this;
    }

    public Map<K, V> build() {
        return data;
    }


}
