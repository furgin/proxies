package au.com.angry.proxy.frames;

import au.com.angry.proxy.Frame;
import au.com.angry.proxy.elements.Elements;

import java.awt.image.BufferedImage;

public class UnhingedLandsFrame implements Frame {
    @Override
    public BufferedImage render(Elements elements) {
        return null;
    }
}
