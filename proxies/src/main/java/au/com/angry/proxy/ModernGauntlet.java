package au.com.angry.proxy;

import java.io.File;
import java.io.IOException;

public class ModernGauntlet {
    public static void main(String[] args) {
        new MapBuilder<String, String>()
//                .with("mono-green-tron", "https://www.mtggoldfish.com/deck/download/943391")
//                .with("5c-humans", "https://www.mtggoldfish.com/deck/download/974097")
//                .with("naya-burn", "https://www.mtggoldfish.com/deck/download/974086")
//                .with("uw-control", "https://www.mtggoldfish.com/deck/download/950385")
                .with("jund", "https://www.mtggoldfish.com/deck/download/994203")
//                .with("affinity", "https://www.mtggoldfish.com/deck/download/972145")
//                .with("hollow-one", "https://www.mtggoldfish.com/deck/download/950386")
//                .with("eldrazi-tron", "https://www.mtggoldfish.com/deck/download/948808")
//                .with("boggles", "https://www.mtggoldfish.com/deck/download/975453")
//                .with("mardu-pyromancer", "https://www.mtggoldfish.com/deck/download/964079")
//                .with("dredge", "https://www.mtggoldfish.com/deck/download/962380")
//                .with("traverse-deaths-shadow", "https://www.mtggoldfish.com/deck/download/962363")
//                .with("lantern", "https://www.mtggoldfish.com/deck/download/943418")
                .build()
                .forEach((key, value) -> {
                    File pdf = new File(key + ".pdf");
                    if (!pdf.exists()) {
                        new Deck.Builder()
                                .resolver(Decks.RESOLVER)
                                .load(value)
                                .build()
                                .generatePDF(key + ".pdf");
                    }
                });
    }
}
