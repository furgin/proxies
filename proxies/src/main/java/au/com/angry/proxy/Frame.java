package au.com.angry.proxy;

import au.com.angry.proxy.elements.Elements;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;

public interface Frame {
    BufferedImage render(Elements elements);
}
