package au.com.angry.proxy.elements;

import au.com.angry.proxy.ProxyElement;

public class TextElement implements ProxyElement {
    private final String text;

    public TextElement(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

}
