package au.com.angry.proxy.decks;

import au.com.angry.proxy.Cache;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Http {

    private static InputStream open(String url) {

        try {
            String md2 = DigestUtils.md2Hex(url);
            File cachedFile = new File(Cache.getDirectory(), md2);
            if (!cachedFile.exists()) {
                System.out.println("downloading: " + url);
                URL u = new URL(url);
                URLConnection urlConnection = u.openConnection();
                urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
                String contentType = urlConnection.getHeaderField("Content-Type");
                if ("application/zip".equals(contentType)) {
                    String filename = u.getFile().substring(u.getFile().lastIndexOf("/") + 1).replaceAll("\\.zip$", "");
                    ZipInputStream zin = new ZipInputStream(urlConnection.getInputStream());
                    for (ZipEntry e; (e = zin.getNextEntry()) != null; ) {
                        if (e.getName().equals(filename)) {
                            return zin;
                        }
                    }
                    throw new RuntimeException();
                } else {
                    IOUtils.copy(urlConnection.getInputStream(), new FileOutputStream(cachedFile));
                }
            }
            return new FileInputStream(cachedFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public static JsonElement load(String url) {
        return new JsonParser().parse(new InputStreamReader(open(url)));
    }

}
