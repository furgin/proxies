package au.com.angry.proxy.text;

import java.awt.*;
import java.awt.image.BufferedImage;

public class TextRendering {

    private final Font font;
    private final String text;
    private final int width;
    private final Color color;
    private final boolean shadow;

    public TextRendering(Font font, String text, int width, Color color, boolean shadow) {
        this.font = font;
        this.text = text;
        this.width = width;
        this.color = color;
        this.shadow = shadow;
    }

    public BufferedImage render() {
        TypeWriter typeWriter = new TypeWriter(width, (int) (font.getSize2D() * 1.1f), font, shadow);
        for (MagicTextIterator.Element e : new MagicTextIterator(text, color))
            typeWriter = e.write(typeWriter);
        BufferedImage src = typeWriter.getImage();
        System.out.println(src.getWidth() + "x" + src.getHeight());
        return src;
    }

    public TextRendering smaller() {
        return new TextRendering(font.deriveFont(font.getSize2D() - 1f), text, width, color, shadow);
    }
}
