package au.com.angry.proxy.decks;

import au.com.angry.proxy.Deck;
import au.com.angry.proxy.MagicCard;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MWSReader {
    public static void main(String[] args) throws Exception {
        Deck.Builder builder = new Deck.Builder();
        new MWSReader().read(new URL("https://www.mtggoldfish.com/deck/download/943391").openStream(), builder);
        builder.build().generateReport();
    }

    public void read(InputStream in, Deck.Builder builder) throws IOException {
        Pattern mwsPattern = Pattern.compile("(\\d+) *(\\[.*\\])? ?(.*)");
        BufferedReader r = new BufferedReader(new InputStreamReader(in));
        String line;
        boolean sideboard = false;
        while ((line = r.readLine()) != null) {
            line = line.replaceAll("//.*", "");
            line = line.replaceAll("^SB:", "");
            if (line.contains("/"))
                line = line.substring(0, line.indexOf("/"));
            line = line.trim();
            if (line.isEmpty()) {
                sideboard = true;
                continue;
            }
            Matcher matcher = mwsPattern.matcher(line);
            if (matcher.matches()) {
                int count = Integer.parseInt(matcher.group(1));
                String set = matcher.group(2);
                String name = matcher.group(3).trim();
                if (set != null) {
                    set = set.replaceAll("\\[", "");
                    set = set.replaceAll("\\]", "");
                }

                Map<String, String> mwsCodes = new HashMap<>();

                mwsCodes.put("A", "LEA");
                mwsCodes.put("B", "LEB");
                mwsCodes.put("U", "2ED");
                mwsCodes.put("AN", "ARN");
                mwsCodes.put("AQ", "ATQ");
                mwsCodes.put("R", "3ED");
                mwsCodes.put("LG", "LEG");
                mwsCodes.put("DK", "DRK");
                mwsCodes.put("FE", "FEM");
                mwsCodes.put("4E", "4ED");
                mwsCodes.put("CH", "CHR");
                mwsCodes.put("HL", "HML");
                mwsCodes.put("IA", "ICE");
                mwsCodes.put("AL", "ALL");
                mwsCodes.put("CS", "CLD");
                mwsCodes.put("PT", "POR");
                mwsCodes.put("P2", "PO2");
                mwsCodes.put("P3", "PTK");
                mwsCodes.put("MI", "MIR");
                mwsCodes.put("5E", "5ED");
                mwsCodes.put("VI", "VIS");
                mwsCodes.put("WL", "WTH");
                mwsCodes.put("TE", "TMP");
                mwsCodes.put("SH", "STH");
                mwsCodes.put("EX", "EXO");
                mwsCodes.put("UG", "UGL");
                mwsCodes.put("US", "USG");
                mwsCodes.put("6E", "6ED");
                mwsCodes.put("UL", "ULG");
                mwsCodes.put("UD", "UDS");
                mwsCodes.put("MM", "MMQ");
                mwsCodes.put("NE", "NMS");
                mwsCodes.put("PY", "PCY");
                mwsCodes.put("IN", "INV");
                mwsCodes.put("PS", "PLS");
                mwsCodes.put("AP", "APC");
                mwsCodes.put("OD", "ODY");
                mwsCodes.put("7E", "7ED");
                mwsCodes.put("TO", "TOR");
                mwsCodes.put("JU", "JUD");
                mwsCodes.put("ON", "ONS");
                mwsCodes.put("LE", "LGN");
                mwsCodes.put("SC", "SCG");
                mwsCodes.put("8E", "8ED");
                mwsCodes.put("MR", "MRD");
                mwsCodes.put("DS", "DST");
                mwsCodes.put("FD", "5DN");
                mwsCodes.put("UN", "UNH");
                mwsCodes.put("9E", "9ED");
                mwsCodes.put("RV", "RAV");
                mwsCodes.put("GP", "GPT");
                mwsCodes.put("CFX", "CON");

                if (mwsCodes.containsKey(set))
                    set = mwsCodes.get(set);

                if (!sideboard) {
                    if (set != null) {
                        builder.add(count, set, name);
                    } else {
                        builder.add(count, name);
                    }
                } else {
                    if (set != null) {
                        builder.sideboard(count, set, name);
                    } else {
                        builder.sideboard(count, name);
                    }
                }

            } else {
                System.out.println("could not find: " + line);
            }
        }
    }
}
