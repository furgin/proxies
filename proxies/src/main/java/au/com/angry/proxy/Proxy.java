package au.com.angry.proxy;

import au.com.angry.proxy.elements.Elements;
import au.com.angry.proxy.elements.ImageElement;
import au.com.angry.proxy.elements.TextElement;

import java.awt.image.BufferedImage;

public class Proxy {

    private final Elements elements;
    private final Frame frame;

    public Proxy(Frame frame, Elements elements) {
        this.frame = frame;
        this.elements = elements;
    }

    public BufferedImage getImage() {
        return frame.render(elements);
    }

    public Elements getElements() {
        return elements;
    }

    public MagicCard getCard() {
        return new MagicCard(getImage());
    }

    public static class Builder {

        private final Elements elements = new Elements();
        private Frame frame;

        public Proxy build() {
            return new Proxy(frame, elements);
        }

        public Builder withName(String name) {
            elements.put("name", new TextElement(name));
            return this;
        }

        public Builder withFrame(Frame frame) {
            this.frame = frame;
            return this;
        }

        public Builder withRules(String rules) {
            elements.put("rules", new TextElement(rules));
            return this;
        }

        public Builder withFlavor(String flavor) {
            elements.put("flavor", new TextElement(flavor));
            return this;
        }

        public Builder withBackground(BufferedImage image) {
            elements.put("background", new ImageElement(image));
            return this;
        }

        public Builder withType(String type) {
            elements.put("type", new TextElement(type));
            return this;
        }

        public Builder withId(String id) {
            elements.put("id", new TextElement(id));
            return this;
        }
    }
}
