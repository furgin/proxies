package au.com.angry.proxy.text;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.image.BufferedImage;
import java.io.IOException;

import static au.com.angry.proxy.text.Utils.getGraphics2D;

public class Fonts {

    public static final FontRenderContext DEFAULT_FRC = new FontRenderContext(null, false, false);
    public static final Font FONT_MPLANTIN;
    public static final Font FONT_MPLANTINI;
    public static final Font FONT_MPLANTI1;
    public static final Font FONT_MATRIX;
    public static final Font FONT_MONOSPACED;
    public static final Font FONT_COURIER;

    public static final Font BOX_FONT;

    static {
        try {
            FONT_MPLANTIN = Font.createFont(Font.TRUETYPE_FONT, Fonts.class.getClassLoader().getResourceAsStream("fonts/mplantin.ttf"));
            FONT_MPLANTINI = Font.createFont(Font.TRUETYPE_FONT, Fonts.class.getClassLoader().getResourceAsStream("fonts/mplantinit.ttf"));
            FONT_MPLANTI1 = Font.createFont(Font.TRUETYPE_FONT, Fonts.class.getClassLoader().getResourceAsStream("fonts/mplanti1.ttf"));
            FONT_MATRIX = Font.createFont(Font.TRUETYPE_FONT, Fonts.class.getClassLoader().getResourceAsStream("fonts/matrix.ttf"));
            FONT_MONOSPACED = new Font("Monospaced", Font.PLAIN, 12);
            FONT_COURIER = Font.createFont(Font.TRUETYPE_FONT, Fonts.class.getClassLoader().getResourceAsStream("fonts/courier/Courier_Prime.ttf"));
            BOX_FONT = Fonts.FONT_MATRIX.deriveFont(100f);
        } catch (FontFormatException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static FontMetrics getFontMetrics(Font font) {
        BufferedImage tmpImg = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = getGraphics2D(tmpImg);
        g.setFont(font);
        FontMetrics fontMetrics = g.getFontMetrics();
        g.dispose();
        return fontMetrics;
    }
}
